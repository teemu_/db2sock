#ifndef _TIMING_H
#define _TIMING_H

#define USE_TIMINGS 0
#define DISABLE_USELESS_TIMINGS 1

#if USE_TIMINGS
FILE *mytimefile;
#include <time.h>
#include <sys/time.h>
#define tStart(name) ; clock_t t1_##name, t2_##name;\
  long wc1_##name, wc2_##name;\
  struct timeval wc_##name;\
  gettimeofday(&wc_##name, NULL);\
  wc1_##name = (long)wc_##name.tv_sec*1000+(long)wc_##name.tv_usec/1000;\
  t1_##name = clock();

#define tStop(name) t2_##name = clock();\
  gettimeofday(&wc_##name, NULL);\
  wc2_##name = (long)wc_##name.tv_sec*1000+(long)wc_##name.tv_usec/1000;\
  mytimefile = fopen("/tmp/mytimefile.txt", "a");\
  fprintf(mytimefile, "%s CPU: %f REAL: %ld\n", #name, (double)(t2_##name - t1_##name) / CLOCKS_PER_SEC, (wc2_##name - wc1_##name));\
  fclose(mytimefile);

#if DISABLE_USELESS_TIMINGS
#define tStart_(name);
#define tStop_(name);
#else
#define tStart_(name) tStart(name);
#define tStop_(name) tStop(name);
#endif // USE_ALL_TIMINGS
#else

#define tStart(a);
#define tStop(a);
#define tStart_(a);
#define tStop_(a);

#endif // USE_TIMINGS

#endif // _TIMING_H
